N’ayant eu qu’une base théorique sur la notion d’API, ce test technique m’a permis de découvrir comment leur exploitation se traduisait en pratique. Plus précisément sur les API SOAP avec la classe SoapClient. J’ai exploré chacune de ses méthodes et ai compris ce à quoi elles servent.

Cependant, faute de temps, il y a quelques aspects que j’aurais aimé améliorer au niveau du rendu. Pour en citer quelques uns : 

1/ Validation : 
Une validation de la saisie du numéro TVA de façon à ce que la requête ne soit envoyée que lorsque la donnée saisie se compose uniquement de chiffres et/ou n’en dépasse pas un certain nombre.
Avant de rediriger l’utilisateur vers la vue contenant les informations concernant l’entreprise souhaitée, effectuer une vérification au préalable de manière à ce que le numéro TVA existe bien et, s’il n’existe pas, informer l’utilisateur et l’inviter à en saisir un nouveau.

2/ Pérennité et modularité de la solution :
Mettre en place une classe permettant de solliciter l’API et implémenter des méthodes qui y sont propres afin d’en faciliter l’exploitation et l’adapter au besoin.

3/ Choix du Country Code :
Faire appel à une API qui puisse alimenter automatiquement la liste des codes pays. Cela évite de le faire manuellement et prendre le risque de se tromper. Ainsi, en cas de mise à jour, celle-ci se fera sans une intervention au niveau du code.

4/ Présentation : 
Utiliser des éléments plus travaillés afin d’avoir un meilleur rendu visuel. Par exemple, les éléments Bootstrap, qui sont également responsive. Ou encore des éléments Angular/AngularJS.

Pour conclure, cet exercice a pu appuyer les connaissances théoriques qui m’ont été communiquées en cours concernant les API SOAP mais aussi mettre fin aux appréhensions que j’avais par rapport à son utilisation. J’ai aussi hâte d’en apprendre plus sur les différentes fonctionnalités qu’elle proposent.