<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class TVA_Controller extends Controller
{
    /**
     * Page d'accueil du site
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/recherche.html.twig');
    }

     /**
     * Fonction qui récupère le code du pays ainsi que le numéro TVA saisis d'une entreprise
     * puis retourne le résultat avec les informations relatives à l'entreprise
     * @Route("/search", name="tva", methods="GET")
     */
     public function searchAction(Request $request)
    {

        $countryCode = $request->query->get("countryCode");
        $vatNumber = $request->query->get("vatNumber");

        $url = "http://ec.europa.eu/taxation_customs/vies/checkVatService.wsdl";

        $client = new \SoapClient($url);

        $result = $client->checkVatApprox(array('countryCode' => $countryCode,'vatNumber' => $vatNumber));

        return $this->render('default/show.html.twig', [
            'countryCode' => $countryCode,
            'vatNumber' => $vatNumber,
            'types' => $types,
            'functions' => $functions,
            'result' => $result,
        ]);
    }
    
}